#!/bin/bash

set -x

### comment ##
yum install -y git-lfs
if [ -f variables.env ];then
     rm -f variables.env
fi
if [ -f tag.out ];then
     rm -f tag.out
fi
if [ -f taglist.out ];then
     rm -f taglist.out
fi
git tag -l >taglist.out
git describe --abbrev=0 --tags >tag.out 2>&1
TAG=""
DELIMITER="."
TAG_EXISTS=$(grep -i fatal tag.out | wc -l )
echo $TAG_EXISTS
#BASEVERSION=$(cat "Deploy/server-version.properties"|awk -F'[<>]' '/version/{print $3}'|tail -1)
#echo "Printing Baseversion - $BASEVERSION"
if [ "$TAG_EXISTS" != "0" ]; then
     TAG=1
else
     TAG=$(cat tag.out)
     echo "Printing current tag - $TAG"
     let TAG+=1
fi

while [ "$(grep -x $TAG taglist.out)" != "" ]
do
        echo "TAG - $TAG already exists, so auto incrementing "
        let TAG+=1
done

echo "Printing new tag - $TAG"

git config --global user.email "${GITLAB_USER_EMAIL}"
git config --global user.name "${GITLAB_USER_NAME}"
cat .git/config
#TAG="v1.1"
git remote add tag-origin https://muhamedbasketball:${GITLAB_ACCESS_TOKEN}@gitlab.com/${CI_PROJECT_PATH}.git
cat .git/config
git tag -a $TAG $CI_COMMIT_SHA -m "Tag $TAG"
git push tag-origin $TAG
